# vk-bad-bot (самая простая, но плохая реализация)

import vk_api               # vk_api
from random import randint  # генерация рандомного числа
from time import sleep      # задержка

# ключ
key_vk = "KEY"
# подключение к VK
vk = vk_api.VkApi(token=key_vk)

print("start")

# "вечный" цикл
while True:
    
    # получение не прочитанных сообщений
    messages = vk.method("messages.getConversations",{"offset":0,"count":20,"filter":"unread"})
    
    # если не прочитано 1 или более сообщений
    if messages["count"] >= 1:
        
        # id пользователя
        id = messages["items"][0]["last_message"]["from_id"]
        
        # текст сообщения
        body = messages["items"][0]["last_message"]["text"]
        
        # random для отправки сообщения
        r = randint(0,2**64-1)
        
        # если сообщение = "привет"
        if body.lower() == "привет":

            # ответ пользователю - "Привет!"
            vk.method("messages.send",{"peer_id":id,"message":"Привет!","random_id":r})
            
        # иное сообщение
        else:
            
            # ответ пользователю - "Не понимаю."
            vk.method("messages.send",{"peer_id": id,"message":"Не понимаю.","random_id":r})
            
        print("сообщение: ", body.lower())
        
    print("...")
    # задержка для снижения повторных обращений к серверу (1 раз в секунду)
    sleep(1)
